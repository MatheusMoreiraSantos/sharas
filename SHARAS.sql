-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03-Jun-2019 às 02:41
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sharas`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `animal`
--

CREATE TABLE `animal` (
  `animal_id` int(11) NOT NULL,
  `animal_nome` varchar(80) DEFAULT NULL,
  `animal_registro` varchar(20) DEFAULT NULL,
  `animal_proprietario` varchar(80) DEFAULT NULL,
  `animal_criador` varchar(80) DEFAULT NULL,
  `animal_nascimento` date DEFAULT NULL,
  `animal_sexo` varchar(30) DEFAULT NULL,
  `animal_pelagem` varchar(80) DEFAULT NULL,
  `animal_modalidade` varchar(80) DEFAULT NULL,
  `animal_treinador` varchar(80) DEFAULT NULL,
  `animal_veterinario` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `animal`
--

INSERT INTO `animal` (`animal_id`, `animal_nome`, `animal_registro`, `animal_proprietario`, `animal_criador`, `animal_nascimento`, `animal_sexo`, `animal_pelagem`, `animal_modalidade`, `animal_treinador`, `animal_veterinario`) VALUES
(1, 'CAPTAIN COUNTRY', 'P175575', 'MARCELO MOREIRA DOS SANTOS', 'MARCELO MOREIRA DOS SANTOS', '2012-12-29', 'GARANHAO', 'BAIO ', 'REDEAS', 'CT MARCELO MOREIRA', 'NETO CAMARGO'),
(2, 'BUDDY CHIC SON  ', 'P249710', 'MARCELO MOREIRA DOS SANTOS', 'MARCELO MOREIRA DOS SANTOS', '2016-10-13', 'MACHO', 'BAIO AMARILHO', 'REDEAS', 'CT MARCELO MOREIRA', 'NETO CAMARGO'),
(3, 'BELL WHIZ  ', 'P127714', 'ANTÔNIO CARLOS DA SILVA MALTEZ  CRIADOR', 'CARLOS ROBERTO CORÁ', '2008-12-02', 'MATRIZ', 'ALAZAO', 'REPRODUCAO', '-', 'NETO CAMARGO'),
(4, 'teste', 'hhhh', 'Matheus', 'Mathesu', '2017-06-03', 'Matriz', 'alazao', 'redeas', 'CT Marcelo Moreira', 'Neto'),
(7, 'c', 'c', '', '', '2019-06-01', 'c', 'c', '', '', ''),
(8, 'm', 'm', 'm', 'm', '2019-06-01', 'm', 'm', 'm', 'm', 'm'),
(9, 'f', 'f', 'f', 'f', '2019-06-13', 'f', 'f', 'f', 'f', 'f'),
(10, 'f', 'f', 'f', 'f', '2019-06-13', 'f', 'f', 'f', 'f', 'f'),
(11, 's', 's', 's', 'ss', '2019-06-01', 's', 's', 's', 's', 's');

-- --------------------------------------------------------

--
-- Estrutura da tabela `calendario`
--

CREATE TABLE `calendario` (
  `calendario_id` int(11) NOT NULL,
  `calendario_data` date DEFAULT NULL,
  `calendario_anotacao` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `calendario`
--

INSERT INTO `calendario` (`calendario_id`, `calendario_data`, `calendario_anotacao`) VALUES
(1, '2019-05-29', 'teste calendario '),
(2, '2019-05-29', ''),
(3, '2019-05-29', 'licqbefivhqef'),
(6, '2019-05-30', 'terminar trabalho sharas'),
(7, '2019-06-01', 'teste do sistema'),
(9, '2019-06-29', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `cliente_id` int(11) NOT NULL,
  `cliente_none` varchar(45) NOT NULL,
  `cliente_cpf` varchar(11) NOT NULL,
  `cliente_endereco` varchar(100) DEFAULT NULL,
  `cliente_cidade` varchar(45) NOT NULL,
  `cliente_datanasc` date DEFAULT NULL,
  `usu_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `feno`
--

CREATE TABLE `feno` (
  `feno_id` int(11) NOT NULL,
  `feno_quantidade` int(11) DEFAULT NULL,
  `feno_qualidade` int(11) DEFAULT NULL,
  `feno_fornecedor` varchar(80) DEFAULT NULL,
  `feno_valor` float DEFAULT NULL,
  `feno_compra` date DEFAULT NULL,
  `feno_duracao` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `feno`
--

INSERT INTO `feno` (`feno_id`, `feno_quantidade`, `feno_qualidade`, `feno_fornecedor`, `feno_valor`, `feno_compra`, `feno_duracao`) VALUES
(1, 200, 2, 'k', 2500, '2019-05-29', '2019-06-29'),
(2, 0, 1, 'k', 0, '2019-06-01', '2019-06-01'),
(3, 3, 0, 'm', 500, '2019-06-01', '2019-06-01'),
(4, 0, 0, 'c', 350, '2019-06-01', '2019-06-01'),
(5, 45, 1, 'sdsd', 2000, '2019-06-01', '2019-07-06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ferrajamento`
--

CREATE TABLE `ferrajamento` (
  `ferrajamento_id` int(11) NOT NULL,
  `ferrajamento_inicio` date DEFAULT NULL,
  `ferrajamento_termino` date DEFAULT NULL,
  `ferrajamento_ferrador` varchar(80) DEFAULT NULL,
  `ferrajamento_animal` varchar(80) DEFAULT NULL,
  `ferrajamento_valor` float DEFAULT NULL,
  `ferrajamento_descricao` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ferrajamento`
--

INSERT INTO `ferrajamento` (`ferrajamento_id`, `ferrajamento_inicio`, `ferrajamento_termino`, `ferrajamento_ferrador`, `ferrajamento_animal`, `ferrajamento_valor`, `ferrajamento_descricao`) VALUES
(1, '2019-05-27', '2019-05-28', 'Ivan Correia', 'MR CHIC DUNT IT', 120, 'Ferrajamento Normal '),
(2, '2019-06-01', '2019-06-01', 'z', 'z', 0, ''),
(3, '2019-06-01', '2019-06-01', 'c', 'c', 0, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `log`
--

CREATE TABLE `log` (
  `log_id` int(11) NOT NULL,
  `log_acao` varchar(20) DEFAULT NULL,
  `log_entidade` varchar(80) DEFAULT NULL,
  `log_usuario` varchar(45) DEFAULT NULL,
  `log_time` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `manutencao`
--

CREATE TABLE `manutencao` (
  `manutencao_id` int(11) NOT NULL,
  `manutencao_nome` varchar(80) DEFAULT NULL,
  `manutencao_salario` float DEFAULT NULL,
  `manutencao_funcao` varchar(500) DEFAULT NULL,
  `manutencao_setor` int(11) DEFAULT NULL,
  `manutencao_anotacao` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `manutencao`
--

INSERT INTO `manutencao` (`manutencao_id`, `manutencao_nome`, `manutencao_salario`, `manutencao_funcao`, `manutencao_setor`, `manutencao_anotacao`) VALUES
(5, 'Caio Henrique Cerri Romano', 1500, 'Limpeza das baias,\nAlimentação dos cavalos', 1, 'Rodara a potra de leilao 1h em trote elevado'),
(6, 'Matheus', 0, 'Sempre que possivel selar cavalos', 1, 'Rodar o Buddy chic son'),
(7, 'z', 0, 'z', 0, ''),
(8, 'c', 0, 'c', 1, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pastagem`
--

CREATE TABLE `pastagem` (
  `pastagem_id` int(11) NOT NULL,
  `pastagem_numero` int(11) DEFAULT NULL,
  `pastagem_quantidade` int(11) DEFAULT NULL,
  `pastagem_qualidade` int(11) DEFAULT NULL,
  `pastagem_anotacao` varchar(500) DEFAULT NULL,
  `pastagem_data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pastagem`
--

INSERT INTO `pastagem` (`pastagem_id`, `pastagem_numero`, `pastagem_quantidade`, `pastagem_qualidade`, `pastagem_anotacao`, `pastagem_data`) VALUES
(12, 1, 8, 2, 'foi passado abubo dia 22/05/2019', '2019-05-26'),
(13, 1, 0, 1, '', '2019-06-01'),
(14, 2, 0, 1, '', '2019-06-01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `racao`
--

CREATE TABLE `racao` (
  `racao_id` int(11) NOT NULL,
  `racao_compra` date DEFAULT NULL,
  `racao_duracao` date DEFAULT NULL,
  `racao_fornecedor` varchar(80) DEFAULT NULL,
  `racao_quantidade` int(11) DEFAULT NULL,
  `racao_valor` float DEFAULT NULL,
  `racao_descricao` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `racao`
--

INSERT INTO `racao` (`racao_id`, `racao_compra`, `racao_duracao`, `racao_fornecedor`, `racao_quantidade`, `racao_valor`, `racao_descricao`) VALUES
(1, '2019-05-30', '2019-05-31', 'Agrogiro', 50, 2100, 'teste'),
(2, '2019-06-01', '2019-06-01', 'm', 0, 750, ''),
(3, '2019-06-01', '2019-06-01', 'c', 0, 800, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `serragem`
--

CREATE TABLE `serragem` (
  `serragem_id` int(11) NOT NULL,
  `serragem_compra` date DEFAULT NULL,
  `serragem_tipo` varchar(80) DEFAULT NULL,
  `serragem_fornecedor` varchar(80) DEFAULT NULL,
  `serragem_metros` float DEFAULT NULL,
  `serragem_baias` int(11) DEFAULT NULL,
  `serragem_duracao` date DEFAULT NULL,
  `serragem_valor` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `serragem`
--

INSERT INTO `serragem` (`serragem_id`, `serragem_compra`, `serragem_tipo`, `serragem_fornecedor`, `serragem_metros`, `serragem_baias`, `serragem_duracao`, `serragem_valor`) VALUES
(2, '2019-05-28', 'granulada', 'José Oliveira', 45, 16, '2019-06-28', 800),
(3, '2019-06-01', 'm', 'm', 2, 2, '2019-06-01', 750),
(4, '2019-06-01', 'c', 'c', 0, 0, '2019-06-01', 365);

-- --------------------------------------------------------

--
-- Estrutura da tabela `treinamento`
--

CREATE TABLE `treinamento` (
  `treinamento_id` int(11) NOT NULL,
  `treinamento_treinador` varchar(80) DEFAULT NULL,
  `treinamento_animal` varchar(80) DEFAULT NULL,
  `treinamento_inicio` date DEFAULT NULL,
  `treinamento_termino` date DEFAULT NULL,
  `treinamento_modalidade` varchar(80) DEFAULT NULL,
  `treinamento_valor` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `treinamento`
--

INSERT INTO `treinamento` (`treinamento_id`, `treinamento_treinador`, `treinamento_animal`, `treinamento_inicio`, `treinamento_termino`, `treinamento_modalidade`, `treinamento_valor`) VALUES
(9, 'CT Marcelo Moreira', 'Buddy Chic Son', '2018-10-27', '2022-05-27', 'REDEAS', 600),
(10, 'dddd', 'dddd', '2019-06-01', '2019-06-01', 'ddd', 500),
(11, 'x', 'x', '2019-06-01', '2019-06-01', 'x', 3658),
(13, 'c', 'c', '2019-06-01', '2019-06-01', 'c', 350),
(14, 'ttt', 'teste', '2019-06-01', '2020-06-06', 'redeas', 5000);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `usu_id` int(11) NOT NULL,
  `usu_login` varchar(50) DEFAULT NULL,
  `usu_senha` varchar(50) DEFAULT NULL,
  `usu_tipo` int(11) DEFAULT NULL,
  `usuario_status` int(11) DEFAULT NULL,
  `usuario_notificacao` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`usu_id`, `usu_login`, `usu_senha`, `usu_tipo`, `usuario_status`, `usuario_notificacao`) VALUES
(4, 'matheus', '698dc19d489c4e4db73e28a713eab07b', 1, 1, 'Diego Moraes esqueceu a senha'),
(5, 'maikon', '698dc19d489c4e4db73e28a713eab07b', 3, 1, 'testar o  sistema\nkdhc'),
(6, 'nicole', '698dc19d489c4e4db73e28a713eab07b', 0, 1, ''),
(7, 'c', '67c762276bced09ee4df0ed537d164ea', 0, 1, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `veterinario`
--

CREATE TABLE `veterinario` (
  `veterinario_id` int(11) NOT NULL,
  `veterinario_animal` varchar(80) DEFAULT NULL,
  `veterinario_nome` varchar(80) DEFAULT NULL,
  `veterinario_inicio` date DEFAULT NULL,
  `veterinario_termino` date DEFAULT NULL,
  `veterinario_diagnostico` varchar(500) DEFAULT NULL,
  `veterinario_tratamento` varchar(500) DEFAULT NULL,
  `veterinario_valor` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `veterinario`
--

INSERT INTO `veterinario` (`veterinario_id`, `veterinario_animal`, `veterinario_nome`, `veterinario_inicio`, `veterinario_termino`, `veterinario_diagnostico`, `veterinario_tratamento`, `veterinario_valor`) VALUES
(2, 'survivor', 'Equino Center', '2019-05-27', '2019-05-31', 'leisão na navicular', 'gelo nas mãos 1 vez ao dia durante 30min\ndurante 10 dais.', 500),
(3, 'm', 'm', '2019-06-01', '2019-06-01', '', '', 0),
(4, 'c', 'c', '2019-06-01', '2019-06-01', '', '', 780);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_cliente_usuario`
-- (See below for the actual view)
--
CREATE TABLE `vw_cliente_usuario` (
`NomeCliente` varchar(45)
,`NomeUsuario` varchar(50)
);

-- --------------------------------------------------------

--
-- Structure for view `vw_cliente_usuario`
--
DROP TABLE IF EXISTS `vw_cliente_usuario`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_cliente_usuario`  AS  select `cliente`.`cliente_none` AS `NomeCliente`,`usuario`.`usu_login` AS `NomeUsuario` from (`cliente` join `usuario` on((`cliente`.`usu_id` = `usuario`.`usu_id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `animal`
--
ALTER TABLE `animal`
  ADD PRIMARY KEY (`animal_id`);

--
-- Indexes for table `calendario`
--
ALTER TABLE `calendario`
  ADD PRIMARY KEY (`calendario_id`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`cliente_cpf`),
  ADD UNIQUE KEY `cliente_cpf_UNIQUE` (`cliente_cpf`),
  ADD KEY `usu_id` (`usu_id`);

--
-- Indexes for table `feno`
--
ALTER TABLE `feno`
  ADD PRIMARY KEY (`feno_id`);

--
-- Indexes for table `ferrajamento`
--
ALTER TABLE `ferrajamento`
  ADD PRIMARY KEY (`ferrajamento_id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `manutencao`
--
ALTER TABLE `manutencao`
  ADD PRIMARY KEY (`manutencao_id`);

--
-- Indexes for table `pastagem`
--
ALTER TABLE `pastagem`
  ADD PRIMARY KEY (`pastagem_id`);

--
-- Indexes for table `racao`
--
ALTER TABLE `racao`
  ADD PRIMARY KEY (`racao_id`);

--
-- Indexes for table `serragem`
--
ALTER TABLE `serragem`
  ADD PRIMARY KEY (`serragem_id`);

--
-- Indexes for table `treinamento`
--
ALTER TABLE `treinamento`
  ADD PRIMARY KEY (`treinamento_id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usu_id`);

--
-- Indexes for table `veterinario`
--
ALTER TABLE `veterinario`
  ADD PRIMARY KEY (`veterinario_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `animal`
--
ALTER TABLE `animal`
  MODIFY `animal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `calendario`
--
ALTER TABLE `calendario`
  MODIFY `calendario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `feno`
--
ALTER TABLE `feno`
  MODIFY `feno_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ferrajamento`
--
ALTER TABLE `ferrajamento`
  MODIFY `ferrajamento_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `manutencao`
--
ALTER TABLE `manutencao`
  MODIFY `manutencao_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pastagem`
--
ALTER TABLE `pastagem`
  MODIFY `pastagem_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `racao`
--
ALTER TABLE `racao`
  MODIFY `racao_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `serragem`
--
ALTER TABLE `serragem`
  MODIFY `serragem_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `treinamento`
--
ALTER TABLE `treinamento`
  MODIFY `treinamento_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `veterinario`
--
ALTER TABLE `veterinario`
  MODIFY `veterinario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`usu_id`) REFERENCES `usuario` (`usu_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
